# coding: utf-8
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='api/index.html'), name="api_index"),
    url(r'^patch', views.SubmitPatch.as_view(), name="api_patch"),
    url(r'^tag/search/(?P<query>[^/]+)?$', views.tag_search_view, name="api_tag_search"),
]
