from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from django.utils.translation import ugettext_lazy as _
from backend import models as bmodels
from aptxapianindex.axi import AptXapianIndex
import xapian
import re
import math
import datetime

class TagFilter(xapian.ExpandDecider):
    def __call__(self, term):
        return term.startswith(b"XT")

class ToplevelTagFilter(xapian.ExpandDecider):
    re_toplevel = re.compile("^XT(?:role|interface|works-with|use)::")
    def __call__(self, term):
        return self.re_toplevel.match(term)

# Built-in choice of result filters
FILTERS = (
    ("default", dict(
        desc=_("Default"),
        xq=xapian.Query(xapian.Query.OP_OR,
                               [b"XTrole::program", b"XTrole::plugin", b"XTrole::devel-lib", b"XTrole::documentation",
                                b"XTrole::metapackage", b"XTrole::source"]),
    )),
    ("desktop", dict(
        desc=_("Desktop apps"),
        xq=xapian.Query(b"XD"),
    )),
    ("programs", dict(
        desc=_("Programs"),
        xq=xapian.Query(xapian.Query.OP_OR, [b"XTrole::program", b"XTrole::metapackage"]),
    )),
    ("all", dict(
        desc=_("All packages"),
        xq=None,
    )),
)
FILTERS_DICT = dict(FILTERS)

class Tagstats(xapian.MatchSpy):
    def __init__(self):
        super(Tagstats, self).__init__()
        self.stats = {}

    def __call__(self, doc, w):
        for t in doc.termlist():
            if t.term.startswith(b"XT"):
                tag = t.term[2:].decode("utf8")
                self.stats[tag] = self.stats.get(tag, 0) + 1


def search_query(query, qfilter=None, whitelist=[], blacklist=[], count_packages=20, count_tags=10):
    axi = AptXapianIndex()

    q, qp = axi.query(query)

    if qfilter not in FILTERS_DICT:
        qfilter = "default"
    xq = FILTERS_DICT[qfilter]["xq"]

    if xq is not None:
        if q is None:
            q = xq
        else:
            q = xapian.Query(xapian.Query.OP_AND, q, xq)

    if whitelist:
        sq = xapian.Query(xapian.Query.OP_AND, [b"XT"+x.encode("utf8") for x in whitelist])
        if q is None:
            q = sq
        else:
            q = xapian.Query(xapian.Query.OP_AND, q, sq)

    if blacklist:
        sq = xapian.Query(xapian.Query.OP_AND, [b"XT"+x.encode("utf8") for x in blacklist])
        if q is None:
            q = xapian.Query.MatchAll
        q = xapian.Query(xapian.Query.OP_AND_NOT, q, sq)

    # Build the enquire with the query
    enquire = xapian.Enquire(axi.db)
    enquire.set_query(q)
    tagstats = Tagstats()
    enquire.add_matchspy(tagstats)

    mset = enquire.get_mset(0, count_packages)

    # Fill up query results
    res = dict()
    res["query"] = query
    res["qfilter"] = qfilter
    res["wl"] = whitelist
    res["bl"] = blacklist
    res["spelling"] = qp.get_corrected_query_string()
    res["match_estimated"] = mset.get_matches_estimated()
    res["matches"] = [dict(pkg=m.document.get_data().decode("utf8"), perc=m.percent) for m in mset]

    # Resolve package names into package objects
    pkgdb = dict((pkg.name, pkg) for pkg in bmodels.Package.by_names(x["pkg"] for x in res["matches"]))
    for i in res["matches"]:
        i["pkg"] = pkgdb[i["pkg"]]

    # Resolve tag names into tag objects, normalise weights, group by facet
    tagdb = dict((tag.name, tag) for tag in bmodels.Tag.by_names(tagstats.stats.keys()))
    tags = []
    for name, count in tagstats.stats.items():
        # Get the Tag object
        tag = tagdb.get(name, None)
        if tag is None: continue
        tags.append(dict(tag=tag, w=count, sel=(name in whitelist)))
    res["tags"] = tags
    return res
