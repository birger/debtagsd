# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
import backend.housekeeping as bh
from debdata.utils import run_cmd
import os
import os.path
import subprocess
import logging

log = logging.getLogger(__name__)

STAGES = ["main", "autotag", "exports", "stats"]


class Index(hk.Task):
    """
    Build a whitelist of packages that should be tagged by robots
    """
    DEPENDS = [bh.ExportTags]

    def run_exports(self, stage):
        try:
            env = dict(os.environ)

            with open("data/all-merged", "wb") as fd:
                subprocess.check_call(["gzip", "-cd", "data/all-merged.gz"], stdout=fd)

            env.update(
                AXI_DB_PATH=os.path.abspath("data/axi"),
                AXI_CACHE_PATH=os.path.abspath("data/axi"),
                AXI_DEBTAGS_DB=os.path.abspath("data/tags-stable"),
            )

            # Look for apt-xapian-index
            uaxi = "/usr/sbin/update-apt-xapian-index"
            if not os.path.exists(uaxi):
                # Fallback on the one locally installed
                axidir = os.path.abspath("../apt-xapian-index")
                uaxi = os.path.join(axidir, "update-apt-xapian-index")
                env["AXI_PLUGIN_DIR"] = os.path.join(axidir, "plugins")

            run_cmd(log, [uaxi, "--force", "--quiet", "--pkgfile=data/all-merged"], env=env)
        finally:
            os.unlink("data/all-merged")
