from django.db import models
from django.core.cache import cache
import backend.models as bmodels
from io import StringIO

def chart_data_tag_count(tagdb, outlier_cutoff=20):
    """
    Compute chart data for a graph of count of packages by number of tags, with
    bars splitting robot/human maintained.
    """
    # Count tag counts
    count_human = dict()
    count_robot = dict()
    outliers = dict()
    max_count = 0
    for pkg, tags in tagdb.iter_packages_tags():
        count = len(tags)
        if count > outlier_cutoff:
            outliers.setdefault(count, []).append(pkg)
            continue
        if "special::unreviewed" in tags:
            h = count_robot
            count -= 1
        else:
            h = count_human
        h[count] = h.get(count, 0) + 1
        if count > max_count:
            max_count = count

    # Create dataset for plotting
    series_robot = []
    series_human = []
    for i in range(max_count + 1):
        series_robot.append((i, count_robot.get(i, 0)))
        series_human.append((i, count_human.get(i, 0)))

    return series_human, series_robot, outliers
