from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import textwrap
import tempfile
import os.path
import subprocess
from io import StringIO

# From http://code.activestate.com/recipes/363602-lazy-property-evaluation/
class lazy_property(object):
    """
    Lazily evaluated const member
    """
    def __init__(self, calculate_function):
        self._calculate = calculate_function

    def __get__(self, obj, _=None):
        if obj is None:
            return self
        value = self._calculate(obj)
        setattr(obj, self._calculate.func_name, value)
        return value


class atomic_writer(object):
    """
    Atomically write to a file
    """
    def __init__(self, fname, mode="w+b", chmod=0o664, sync=True, **kw):
        self.fname = fname
        self.chmod = chmod
        self.sync = sync
        dirname = os.path.dirname(self.fname)
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        self.fd, self.abspath = tempfile.mkstemp(dir=dirname, text="b" not in mode)
        self.outfd = open(self.fd, mode, closefd=True, **kw)

    def __enter__(self):
        return self.outfd

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.outfd.flush()
            if self.sync: os.fdatasync(self.fd)
            os.fchmod(self.fd, self.chmod)
            os.rename(self.abspath, self.fname)
        else:
            os.unlink(self.abspath)
        self.outfd.close()
        return False


def run_cmd(log, cmd, **kw):
    from six.moves import shlex_quote
    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kw)
    out, err = proc.communicate()
    res = proc.wait()
    if res != 0:
        cmdfmt = " ".join(shlex_quote(x) for x in cmd)
        log.error("%s failed with code %d. stdout: %r, stderr: %r", cmdfmt, res, out, err)
        raise RuntimeError("{} failed with code {}".format(cmdfmt, res))

def splitdesc(text):
    if text is None:
        return "", ""
    desc = text.split("\n", 1)
    if len(desc) == 2:
        return desc[0], textwrap.dedent(desc[1])
    else:
        return desc[0], ""

def tags_to_facets(seq):
    """
    Convert a sequence of tags into a sequence of facets.

    Note that no attempt is made to remove duplicates from the facet sequence.
    """
    for t in seq:
        yield t.split("::")[0]

class Sequence(object):
    def __init__(self):
        self.val = 0
    def next(self):
        self.val += 1
        return self.val

class HTMLDescriptionRenderer(object):
    def __init__(self):
        self.output = StringIO()
        self.cur_item = None

    def add_line(self, line):
        if len(line) < 1: return

        if line[0] == ".":
            if len(line) == 1:
                self.add_emptyline()
            else:
                return
        elif line[0].isspace():
            self.add_verbatim(line)
        else:
            self.add_text(line)

    def add_description(self, desc):
        for line in desc.split("\n"):
            self.add_line(line)
        self.done()
        return self.output.getvalue()

    def _open_item(self, name):
        """
        If there is an item which is not @name, close it, then open item @name

        Returns True if the item is just open, False if we continue it
        """
        if self.cur_item != name:
            if self.cur_item is not None:
                print("</{}>".format(self.cur_item), file=self.output)
            self.cur_item = name
            print("<{}>".format(self.cur_item), file=self.output)
            return True
        return False

    def _close_item(self):
        if self.cur_item is not None:
            print("</{}>".format(self.cur_item), file=self.output)
            self.cur_item = None

    def add_emptyline(self):
        # close <p> or <pre>
        self._close_item()

    def add_verbatim(self, line):
        self._open_item("pre")
        print(line, file=self.output)

    def add_text(self, line):
        if self._open_item("p"):
            self.output.write(line.strip())
        else:
            self.output.write(" %s" % line.strip())

    def done(self):
        self._close_item()

    @classmethod
    def format(cls, desc):
        return cls().add_description(desc)
