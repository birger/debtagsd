# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
import backend.housekeeping as bh
import django.db
import backend.models as bmodels
from debdata import patches
from collections import defaultdict
import re
import logging

log = logging.getLogger(__name__)

STAGES = ["main", "autotag", "checks", "stats"]


class Autotag(hk.Task):
    """
    Build a whitelist of packages that should be tagged by robots
    """
    NAME = "autotag"

    def __init__(self, *args, **kw):
        super(Autotag, self).__init__(*args, **kw)
        self.whitelist = {}
        self.db = bmodels.Patches()

    @django.db.transaction.atomic
    def apply(self, name, patchset):
        simplified = patchset.simplified(self.tagdb)

        count_pkgs = 0
        if simplified and not simplified.empty():
            count_pkgs = len(patchset)
            self.db.apply_maint_patchset("autotag_" + name, simplified)

        log.info("%d %s patches applied to special::unreviewed packages", count_pkgs, name)

    def run_autotag(self, stage):
        unreviewed = bmodels.Tag.objects.get(name="special::unreviewed")

        # Build the whitelist of packages that are maintained by robots
        self.whitelist = frozenset(unreviewed.stable_packages.values_list("name", flat=True))

        # Get a string-based tag database
        self.tagdb = bmodels.tag_debtags_db()


class Sections(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        for p in self.hk.binpackages.by_section.get("libdevel", ()):
            if p.name not in whitelist: continue
            patchset.add(p.name, frozenset(("role::devel-lib", "devel::library")))

        # TODO: section games
        # TODO: section cli-mono
        # TODO: section admin
        # TODO: section database
        # TODO: section devel
        # TODO: section doc
        # TODO: section editors
        # TODO: section embedded (hardware)
        # TODO: section fonts
        # TODO: section gnu-r
        # ...and so on from https://packages.debian.org/unstable/

        self.hk.autotag.apply("sections", patchset)


class UIToolkit(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        re_maps = (
            (re.compile(r"^libgtk"), "uitoolkit::gtk"),
            (re.compile(r"^libqt[0-9]"), "uitoolkit::qt"),
            (re.compile(r"^libsdl[0-9]"), "uitoolkit::sdl"),
            (re.compile(r"^lesstif[12]"), "uitoolkit::motif"),
            (re.compile(r"^libncurses"), "uitoolkit::ncurses"),
            (re.compile(r"^libwxgtk"), "uitoolkit::wxwidgets"),
        )

        for name, pkg in self.hk.binpackages.by_name.items():
            # Skip libraries
            if name not in whitelist: continue
            if pkg.sec.startswith("lib"): continue

            added = set()
            for dep in pkg.predeps, pkg.deps:
                for deppkg in dep:
                    for regexp, newtag in re_maps:
                        if regexp.match(deppkg):
                            added.add(newtag)
                            break
            patchset.add(name, added)

        self.hk.autotag.apply("uitoolkit", patchset)


class Kernel(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        rules = (
            ("devel", (
                ("linux-headers-", ("admin::kernel", "devel::lang:c", "devel::library", "implemented-in::c", "role::devel-lib")),
                ("linux-kbuild-", ("admin::kernel", "implemented-in::c", "implemented-in::perl", "implemented-in::shell")), # role::???
                ("linux-source-", ("admin::kernel", "implemented-in::c", "role::source")),
                ("linux-support-", ("admin::kernel", "devel::lang:c", "devel::library", "implemented-in::c", "role::devel-lib")),
                ("linux-tree-", ("admin::kernel", "role::dummy", "special::meta")),
            )),
            ("doc", (
                ("linux-doc-", ("admin::kernel", "made-of::html", "role::documentation")),
                ("linux-manual-", ("admin::kernel", "made-of::man", "role::documentation")),
            )),
            ("admin", (
                ("linux-image-", ("admin::kernel", "implemented-in::c")), # +role::???
                ("linux-patch-", ("admin::kernel", "role::source")),
            )),
        )

        for section, section_rules in rules:
            for p in self.hk.binpackages.by_section.get(section, ()):
                name = p.name
                if name not in whitelist: continue
                added = set()
                for prefix, tags in section_rules:
                    if name.startswith(prefix):
                        added.update(tags)
                        break
                patchset.add(name, added)

        self.hk.autotag.apply("kernel", patchset)


class Names(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        re_maps = (
            (re.compile("^libmono[0-9-].+-cil$"), ("devel::library", "role::devel-lib", "devel::ecma-cli")),
        )

        for name, pkg in self.hk.binpackages.by_name.items():
            if name not in whitelist: continue
            added = set()
            for regexp, tags in re_maps:
                if regexp.match(name):
                    added.update(tags)
                    break
                patchset.add(name, added)

        self.hk.autotag.apply("names", patchset)


class Perl(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        re_perllib = re.compile("^lib.+-perl$")

        for p in self.hk.binpackages.by_section.get("perl", ()):
            if p.name not in whitelist: continue
            if not re_perllib.match(p.name): continue
            added = set(("devel::lang:perl", "devel::library"))
            if "all" in p.archs:
                added.add("implemented-in::perl")
            else:
                added.add("implemented-in::c")
                patchset.add(p.name, added)

        self.hk.autotag.apply("perl", patchset)


class NewVersions(hk.Task):
    DEPENDS = [bh.BinPackages, Autotag]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        stem_regexps = (
            # Shared libraries
            re.compile(r"^lib(.+?)[0-9.]+$"),
            # Kernel modules
            re.compile(r"^(.+)-modules-[0-9.-]+"),
        )

        # Return the stemmed version of the package name, or None if the
        # package name is not one we handle
        def stem(name):
            for r in stem_regexps:
                mo = r.match(name)
                if mo: return mo.group(1)
            return None

        # Group package names by their stemmed version
        by_stem = defaultdict(list)
        for name in self.hk.binpackages.by_name.keys():
            if name not in whitelist: continue
            stemmed = stem(name)
            if stemmed is None: continue
            by_stem[stemmed].append(name)

        # Get the unstable tag database
        db = self.hk.autotag.tagdb

        # Go through every group with more than 1 member, merging all the tags
        for group in by_stem.values():
            if len(group) < 2: continue

            # Compute the merged tag set
            merged_tags = set()
            for p in group:
                for t in db.tags_of_package(p):
                    if t.startswith("special::"): continue
                    merged_tags.add(t)

            # In case there were only special tags, we have nothing to do
            if not merged_tags: continue

            # Add tags from the merged tag set to all packages in the group with
            # unreviewed tags
            for pkg in group:
                tags = db.tags_of_package(pkg)

                # Add the tags
                added = merged_tags - tags
                if added:
                    patchset.add(pkg, added)

        self.hk.autotag.apply("newversions", patchset)


class Apriori(hk.Task):
    DEPENDS = [Autotag, bh.Apriori]

    def run_autotag(self, stage):
        whitelist = self.hk.autotag.whitelist
        patchset = patches.PatchSet()

        # We cannot evaluate facet rules, because they give facet suggestions,
        # not actual tags to add
        rules = self.hk.apriori.tag_rules

        db = self.hk.autotag.tagdb

        # Evaluate tag rules
        for pkg, tags in db.iter_packages_tags():
            if pkg not in whitelist: continue

            added = set()
            for r in rules:
                if r.src.issubset(tags) and r.tgt not in tags:
                    added.add(r.tgt)
            if added:
                patchset.add(pkg, added)

        self.hk.autotag.apply("apriori", patchset)
