from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from debdata import utils
import re

class CheckEngine(object):
    TAGCHECKERS = [None] * 10

    @classmethod
    def register(cls, chk):
        """
        Register a tag checker in the system
        """
        fid = chk.ID
        if fid >= len(cls.TAGCHECKERS):
            # Extend if needed
            cls.TAGCHECKERS.extend([None] * (fid - len(cls.TAGCHECKERS) + 5))
        if cls.TAGCHECKERS[fid] is not None:
            raise KeyError("Tagchecker ID %d already in use for %s" % (fid, cls.TAGCHECKERS[fid].__name__))
        cls.TAGCHECKERS[fid] = chk

    @classmethod
    def by_id(cls, fid):
        """
        Return the tag checker for the given ID
        """
        if fid < len(cls.TAGCHECKERS):
            return cls.TAGCHECKERS[fid]
        return None

    @classmethod
    def list(cls):
        for t in cls.TAGCHECKERS:
            if t is not None:
                yield t

    def __init__(self):
        """
        Initialize the check runner
        """
        self.checks = []

    def refresh(self):
        """
        Load / reload tag checkers at the start of a check run
        """
        # FIXME: call a refresh() method on the checkers instead, so that
        # checkers that need no loaded state can just do nothing, and checkers
        # that load a data file can skip reloading it if it has not changed
        self.checks = []
        for cls in self.TAGCHECKERS:
            if cls is None: continue
            self.checks.append(cls())

    def run(self, tags):
        """
        Run all available checks on the given tagset, generating a sequence of
        (check object, check results) for each check that failed.
        """
        for c in self.checks:
            for res in c.check_tags(tags):
                yield c, res

engine = CheckEngine()

class Tagcheck(object):
    ID = None
    NAME = None  # Check name used to identify this check
    SDESC = None # One line short description
    LDESC = None # Multiline long description
    JS = None
    JS_DATA = None

    @classmethod
    def name(cls):
        res = getattr(cls, "NAME", None)
        if res is None:
            res = cls.__name__
            if res.endswith("Tagcheck"):
                res = res[:-8]
        return res

    @classmethod
    def sdesc(cls):
        return getattr(cls, "SDESC", None)

    @classmethod
    def ldesc(cls):
        return getattr(cls, "LDESC", None)

    @classmethod
    def js_data(cls):
        """
        Return a JavaScript snippet that defines global variables used by this checker
        """
        return cls.JS_DATA

    @classmethod
    def js_check(cls):
        """
        Return a JavaScript snippet that runs this checker
        """
        return cls.JS

class HasRoleTagcheck(Tagcheck):
    ID = 1
    SDESC = "Every package should have a role::* tag"
    JS = """
    if (!tags.hasRE(/^role::/))
        add_check("A <i>role::*</i> tag is still missing.");
    """

    def check_tags(self, tags):
        found = False
        for t in tags:
            if t.startswith("role::"):
                found = True
                break
        if not found:
            yield dict()

    @classmethod
    def format(cls, pkg, data):
        return "A <i>role::*</i> tag is still missing."
CheckEngine.register(HasRoleTagcheck)

class HasUIToolkitTagcheck(Tagcheck):
    ID = 2
    SDESC = "Every package with an X11 or 3D interface should have a uitoolkit::* tag"
    JS = """
    if (tags.hasRE(/^(interface::(3d|x11)|x11::application)$/) && !tags.hasRE(/^uitoolkit::/))
        add_check("An <i>uitoolkit::*</i> tag seems to be missing.");
    """

    def check_tags(self, tags):
        has_iface = None
        for t in tags:
            if t.startswith("uitoolkit::"):
                return
        # There is no uitoolkit:: tag
        for t in ["interface::x11", "interface::3d"]:
            if t in tags:
                has_iface = t
                break
        if has_iface is not None:
            yield dict(found=has_iface)

    @classmethod
    def format(cls, pkg, data):
        return "A <i>uitoolkit::*</i> tag seems to be missing," \
               " since the package has interface %s." % data.get("found", "(undefined)")
CheckEngine.register(HasUIToolkitTagcheck)

class UnreviewedTagcheck(Tagcheck):
    ID = 3
    SDESC = "Package tags should be reviewed by humans"
    JS = """
    if (tags.has("special::unreviewed"))
        add_check("The <i>unreviewed</i> tags are still present.");
    """

    def check_tags(self, tags):
        if "special::unreviewed" in tags:
            yield dict()

    @classmethod
    def format(cls, pkg, data):
        return "The <i>unreviewed</i> tag is still present."
CheckEngine.register(UnreviewedTagcheck)

class HasImplementedInTagcheck(Tagcheck):
    ID = 4
    SDESC = "Every package with a role::* of program, devel-lib, plugin, or source, should have an implemented-in::* tag"
    JS = """
    if (tags.hasRE(/^role::(program|devel-lib|plugin|shared-lib|source)$/) && ! tags.hasRE(/^implemented-in::/))
        add_check("An <i>implemented-in::*</i> tag seems to be missing.");
    """

    re_role = re.compile(r"^role::(program|devel-lib|plugin|shared-lib|source)$")

    def check_tags(self, tags):
        is_sw = None
        for t in tags:
            mo = self.re_role.match(t)
            if mo is not None:
                is_sw = mo.group(1)
            elif t.startswith("implemented-in::"):
                return
        if is_sw is not None:
            yield dict(found=is_sw)

    @classmethod
    def format(cls, pkg, data):
        return "An <i>implemented-in::*</i> tag seems to be missing," \
               " since the package has role %s." % data.get("found", "(undefined)")
CheckEngine.register(HasImplementedInTagcheck)

class HasDevelLangTagcheck(Tagcheck):
    ID = 5
    SDESC = "Every development library should have a devel::lang:* tag"
    JS = """
    if (tags.hasRE(/^(role::devel-lib|devel::library)$/) && ! tags.hasRE(/^devel::lang:/))
        add_check("A <i>devel::lang:*</i> tag seems to be missing.");
    """

    def check_tags(self, tags):
        is_devlib = None
        for t in "role::devel-lib", "devel::library":
            if t in tags:
                is_devlib = t
                break
        if is_devlib is None:
            return

        for t in tags:
            if t.startswith("devel::lang:"):
                return

        yield dict(found=is_devlib)

    @classmethod
    def format(cls, pkg, data):
        return "A <i>devel::lang:*</i> tag seems to be missing," \
               " since the package has tag %s." % data.get("found", "(undefined)")
CheckEngine.register(HasDevelLangTagcheck)

class HasEquivsTagcheck(Tagcheck):
    ID = 6
    SDESC = "role::devel-lib and devel::library should always be together"
    JS = """
    if (tags.has("devel::library") && !tags.has("role::devel-lib"))
    {
        var c = add_check("A <i>role::devel-lib</i> tag seems to be missing.");
        c.add_fix("add role::devel-lib", "+role::devel-lib");
    }
    if (!tags.has("devel::library") && tags.has("role::devel-lib"))
    {
        var c = add_check("A <i>devel::library</i> tag seems to be missing.");
        c.add_fix("add devel::library", "+devel::library");
    }
    """

    def check_tags(self, tags):
        has_role = "role::devel-lib" in tags
        has_devlib = "devel::library" in tags
        if has_role and not has_devlib:
            yield dict(has="role::devel-lib", miss="devel::library")
        elif has_devlib and not has_role:
            yield dict(has="devel::library", miss="role::devel-lib")

    @classmethod
    def format(cls, pkg, data):
        return "A <i>%s</i> tag seems to be missing," \
               " since the package has tag %s." % (
                   data.get("miss", "(undefined)"),
                   data.get("has", "(undefined)"))
CheckEngine.register(HasEquivsTagcheck)

class HasGameTagcheck(Tagcheck):
    ID = 7
    SDESC = "Every package with use::gameplaying should have a game::* tags"
    JS = """
    if (tags.has("use::gameplaying") && !tags.hasRE(/^game::/))
        add_check("A <i>game::*</i> tag seems to be missing.");
    """

    def check_tags(self, tags):
        if "use::gameplaying" not in tags:
            return
        for t in tags:
            if t.startswith("game::"):
                return
        yield dict()

    @classmethod
    def format(cls, pkg, data):
        return "A <i>game::*</i> tag seems to be missing," \
               " since the package has tag use::gameplaying."
CheckEngine.register(HasGameTagcheck)

class JSRuleCompressor(object):
    CHARSET0 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    CHARSET1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";

    def __init__(self):
        self.freq = dict()
        self.freqs = []
        self.names = dict()
        self.seq = 0

    def analyse_freq(self, rules):
        for r in rules:
            for t in r.src:
                self.freq[t] = self.freq.get(t, 0) + 1
            self.freq[r.tgt] = self.freq.get(r.tgt, 0) + 1

    def get_name(self):
        res = [];
        val = self.seq;
        first = True
        while True:
            if first:
                chars = self.CHARSET0
                first = False
            else:
                chars = self.CHARSET1
            c = val % len(chars)
            res.append(chars[c])
            val //= len(chars)
            if val == 0: break
        self.seq += 1
        return "".join(res)

    def assign_names(self):
        self.freqs = [(f, k) for k, f in self.freq.items()]
        self.freqs.sort(reverse=True)
        for f, k in self.freqs:
            self.names[k] = self.get_name()

    def js_vars(self):
        res = []
        for f, k in self.freqs:
            res.append("%s='%s'" % (self.names[k], k))
        return "var " + ",".join(res) + ";"

    def js_rules(self, rules):
        # Aggregate by target
        agg_rules = dict()
        for r in rules:
            agg_rules.setdefault(r.tgt, []).append(r)

        # Serialize to compressed javascript code
        jsrules = []
        for tgt, rules in agg_rules.items():
            jstgtrules = []
            for r in rules:
                jstgtrules.append("[%d,%s]" % (round(r.conf), ",".join(self.names[x] for x in r.src)))
            jsrules.append("[%s,%s]" % (self.names[tgt], ",".join(jstgtrules)))

        # Return it as javascript code
        return "[%s]" % ",".join(jsrules)




class AprioriTagcheck(Tagcheck):
    ID = 8
    SDESC = "Tagging checks based on rules computed via a statistical analysys of the reviewed tag database"
    JS = """
(function(){
    var tmp = apriori_rules_eval(tags);
    for (var i in tmp)
        res.push(tmp[i]);
})()
"""

    def __init__(self):
        import backend.models as bmodels
        self.rules = bmodels.load_apriori_ruleset()

    def check_tags(self, tags):
        if self.rules is None: return

        # Evaluate facet rules
        ftags = frozenset(utils.tags_to_facets(tags))
        for r in self.rules["f"]:
            if r.src.issubset(ftags) and r.tgt not in ftags:
                yield dict(
                    t="f",
                    src=list(r.src),
                    tgt=r.tgt,
                    conf=r.conf)

        # Evaluate tag rules
        for r in self.rules["t"]:
            if r.src.issubset(tags) and r.tgt not in tags:
                yield dict(
                    t="t",
                    src=list(r.src),
                    tgt=r.tgt,
                    conf=r.conf)

    @classmethod
    def format(cls, pkg, data):
        if data["t"] == "f":
            if len(data["src"]) > 1:
                return "%d%% of package with tags of facets (%s) also have tags of facet %s" % (
                    int(data["conf"]), ", ".join(data["src"]), data["tgt"])
            else:
                return "%d%% of package with tag of facet %s also have tags of facet %s" % (
                    int(data["conf"]), data["src"][0], data["tgt"])
        elif data["t"] == "t":
            if len(data["src"]) > 1:
                return "%d%% of package with tags (%s) also have tag %s" % (
                    int(data["conf"]), ", ".join(data["src"]), data["tgt"])
            else:
                return "%d%% of package with tag %s also have tag %s" % (
                    int(data["conf"]), data["src"][0], data["tgt"])
        else:
            return "Unknown type of apriori rule check"

    @classmethod
    def js_data(cls):
        import backend.models as bmodels
        rules = bmodels.load_apriori_ruleset()

        if rules is None:
            data = dict(
                fcomp="",
                fsugg="[]",
                negfsugg="[]",
                tcomp="",
                tsugg="[]",
                negtsugg="[]",
            )
        else:
            # Build compressed ruleset serializers
            fcomp = JSRuleCompressor()
            fcomp.analyse_freq(rules["f"])
            fcomp.analyse_freq(rules["nf"])
            fcomp.assign_names()

            tcomp = JSRuleCompressor()
            tcomp.analyse_freq(rules["t"])
            tcomp.analyse_freq(rules["nt"])
            tcomp.assign_names()

            data = dict(
                fcomp=fcomp.js_vars(),
                tcomp=tcomp.js_vars(),
                fsugg=fcomp.js_rules(rules["f"]),
                tsugg=tcomp.js_rules(rules["t"]),
                negfsugg=fcomp.js_rules(rules["nf"]),
                negtsugg=tcomp.js_rules(rules["nt"]),
            )

        try:
            res = """
var apriori_rules = function() {
    var res = {};

    (function() {
        %(fcomp)s
        // Facet correlations
        res.fsugg = %(fsugg)s;
        // Negative facet correlations
        res.negfsugg = %(negfsugg)s;
    })();
    (function() {
        %(tcomp)s
        // Tag correlations
        res.tsugg = %(tsugg)s;
        // Negative tag correlations
        res.negtsugg = %(negtsugg)s;
    })();

    return res;
}();

function apriori_rules_eval(tags) {
    // Items we already suggested, to avoid suggesting things twice
    var suggested = new Object();
    var neg_suggested = new Object();

    // Compute the facet set, and initialise the suggested sets
    var facets = new Tagset();
    tags.each(function(t){
        var s = t.split("::");
        facets.add(s[0]);
        suggested[s[0]] = true;
        suggested[t] = true;
    });

    // Check if a tagset matches the given sources
    // Return the confidence if it matches, else null
    var match_sources = function(tags, src)
    {
        for (var si in src)
        {
            // The first element is the confidence
            if (si == 0) continue;
            if (!tags.has(src[si]))
                return null;
        }
        return src[0];
    }

    // Compute suggestions
    var suggest = function(tags, ruleset) {
        var res = [];
        for (var i in ruleset)
        {
            var rules = ruleset[i];
            var target = rules[0];

            // Skip if we already suggested it
            if (suggested[target]) continue;

            // Check all rules
            for (var ri in rules)
            {
                if (ri == 0) continue;
                var conf = match_sources(tags, rules[ri]);
                if (conf != null)
                {
                    res.push([target, conf]);
                    suggested[target] = true;
                    break;
                }
            }
        }
        return res;
    };

    // Compute reverse suggestions
    var neg_suggest = function(tags, ruleset) {
        var res = [];
        for (var i in ruleset)
        {
            var rules = ruleset[i];
            var target = rules[0];

            // We suggest to remove tags, so skip when they are not present
            if (!tags.has(target)) continue;

            // Skip if we already suggested it
            if (neg_suggested[target]) continue;

            // Check all rules
            for (var ri in rules)
            {
                if (ri == 0) continue;
                var conf = match_sources(tags, rules[ri]);
                if (conf != null)
                {
                    res.push([target, conf]);
                    neg_suggested[target] = true;
                    break;
                }
            }
        }
        return res;
    };

    // Array of resulting suggestions
    var res = new Array();

    var slist = suggest(facets, apriori_rules.fsugg);
    for (var i in slist)
    {
        var tgt = slist[i][0], conf = slist[i][1];
        var c = new Check("hint", 'There is a '+conf+'%% chance that a '+tgt+'::* tag is missing');
        res.push(c);
    }

    slist = suggest(tags, apriori_rules.tsugg);
    for (var i in slist)
    {
        var tgt = slist[i][0], conf = slist[i][1];
        var c = new Check("hint", 'There is a '+conf+'%% chance that the tag '+tgt+' is missing');
        c.add_fix("add tag " + tgt, "+" + tgt);
        res.push(c);
    }

    slist = neg_suggest(facets, apriori_rules.negfsugg);
    for (var i in slist)
    {
        var tgt = slist[i][0], conf = slist[i][1];
        var c = new Check("hint", tgt + "::* tags are rarely ("+conf+"%%) used in this way");
        var to_remove = [];
        var patch = [];
        tags.each(function(el) { if (facet_of_tag(el) == tgt) { to_remove.push(el); patch.push("-" + el); }});
        c.add_fix("remove tags " + to_remove.join(", "), patch.join(","));
        res.push(c);
    }

    slist = neg_suggest(tags, apriori_rules.negtsugg);
    for (var i in slist)
    {
        var tgt = slist[i][0], conf = slist[i][1];
        var c = new Check("hint", tgt + " is rarely ("+conf+"%%) used in this way");
        c.add_fix("remove tag " + tgt, "-" + tgt);
        res.push(c);
    }

    return res;
}
""" % data
        except Exception as e:
            print(e)
        return res
CheckEngine.register(AprioriTagcheck)

